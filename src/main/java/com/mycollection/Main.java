package com.mycollection;

import com.mycollection.one_order_linked_list.OneOrderLinkedList;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Integer> e = new OneOrderLinkedList<>();

        e.add(2);
        e.add(3);
        e.add(4);
        e.add(0);
        e.add(1);

        Iterator <?> iterator = e.iterator();

        while (iterator.hasNext()) {
            System.out.print(iterator.next()+" ");
        }
    }
}
