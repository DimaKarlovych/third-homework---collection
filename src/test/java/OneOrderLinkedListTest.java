import com.mycollection.one_order_linked_list.OneOrderLinkedList;
import org.junit.jupiter.api.Test;

import javax.naming.OperationNotSupportedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OneOrderLinkedListTest {

    private OneOrderLinkedList<Object> oneOrderLinkedList = new OneOrderLinkedList<>();

    @Test
    void sizeShouldBeEqualsToZeroWhenCollectionIsEmpty() {
        assertEquals(0, oneOrderLinkedList.size());
    }

    @Test
    void sizeShouldBeEqualsToElementsCountWhenCollectionIsNotEmpty() {
        oneOrderLinkedList.add("a");
        oneOrderLinkedList.add("b");
        assertEquals(2, oneOrderLinkedList.size());
    }

    @Test
    void shouldBeTrueWhenCollectionIsEmpty() {
        assertTrue(oneOrderLinkedList.isEmpty());
    }

    @Test
    void shouldBeFalseWhenCollectionIsNotEmpty() {
        oneOrderLinkedList.add("A");
        assertFalse(oneOrderLinkedList.isEmpty());
    }

    @Test
    void shouldBeTrueWhenCollectionContainsElement() {
        oneOrderLinkedList.add("a");
        assertTrue(oneOrderLinkedList.contains("a"));
    }

    @Test
    void shouldBeFalseWhenCollectionDoesNotContainElement() {
        oneOrderLinkedList.add("b");
        assertFalse(oneOrderLinkedList.contains("a"));
    }

    @Test
    void shouldBeFalseWhenCollectionDoesNotContainNullElement() {
        oneOrderLinkedList.add("b");
        assertFalse(oneOrderLinkedList.contains(null));
    }

    @Test
    void containsShouldThrowIllegalStateExceptionWhenCollectionIsEmpty() {
        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.contains("a"));
    }

    @Test
    void hasNextShouldReturnFalseWhenCollectionIsEmpty() {
        Iterator <?> iterator = oneOrderLinkedList.iterator();
        assertFalse(iterator.hasNext());
    }

    @Test
    void hasNextShouldReturnTrueWhenCollectionIsNotEmpty() {
        oneOrderLinkedList.add(0);
        Iterator <?> iterator = oneOrderLinkedList.iterator();
        assertTrue(iterator.hasNext());
    }

    @Test
    void iteratorNextShouldReturnNullWhenCollectionIsEmpty() {
        Iterator <?> iterator = oneOrderLinkedList.iterator();
        assertNull(iterator.next());
    }

    @Test
    void iteratorNextShouldBeEqualsToFirstElementOfCollectionWhenItCalledFirstTime() {
        oneOrderLinkedList.add(0);
        Iterator <?> iterator = oneOrderLinkedList.iterator();
        assertEquals(0, iterator.next());
    }

    @Test
    void iteratorNextShouldBeEqualsToNullWhenIteratorIsAfterLastElement() {
        oneOrderLinkedList.add(0);
        Iterator <?> iterator = oneOrderLinkedList.iterator();
        iterator.next();
        assertNull(iterator.next());
    }

    @Test
    void shouldReturnEmptyArrayWhenEmptyCollectionCalled() {
        assertEquals("[]", Arrays.toString(oneOrderLinkedList.toArray()));
    }

    @Test
    void toArrayShouldReturnOneElementArrayWhenCollectionWithOneElementCalled() {
        oneOrderLinkedList.add(1);
        assertEquals("[1]", Arrays.toString(oneOrderLinkedList.toArray()));
    }

    @Test
    void toArrayShouldReturnAllElementsFromCalledCollection() {
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);
        oneOrderLinkedList.add(3);
        oneOrderLinkedList.add(4);
        assertEquals("[1, 2, 3, 4]", Arrays.toString(oneOrderLinkedList.toArray()));
    }

    @Test
    void toArrayWithParameterShouldReturnOneElementArrayWhenCollectionWithOneElementPassed() {
        Object [] objects = new Object[1];
        oneOrderLinkedList.add(0);
        assertEquals("[0]", Arrays.toString(oneOrderLinkedList.toArray(objects)));
    }

    @Test
    void toArrayWithParameterShouldReturnEmptyArrayWhenEmptyCollectionPassed() {
        Object [] objects = new Object[0];
        oneOrderLinkedList.add(0);
        assertEquals("[]", Arrays.toString(oneOrderLinkedList.toArray(objects)));
    }

    @Test
    void toArrayWithParameterShouldReturnEmptyArrayWhenNullCollectionPassed() {
        Object [] objects = null;

        assertThrows(NullPointerException.class, () -> {
            Arrays.toString(oneOrderLinkedList.toArray(objects));
        });
    }

    @Test
    void toArrayWithParameterShouldReturnArrayEqualsOfPassedCollection() {
        Object [] objects = new Object[2];
        oneOrderLinkedList.add(0);
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);
        oneOrderLinkedList.add(3);
        assertEquals("[0, 1]", Arrays.toString(oneOrderLinkedList.toArray(objects)));
    }

    @Test
    void toArrayWithParameterShouldThrowIndexOutOfBoundsExceptionWhenArrayBiggerThanCollection() {
        Object [] objects = new Object[5];
        oneOrderLinkedList.add(0);
        oneOrderLinkedList.add(1);

        assertThrows(ArrayIndexOutOfBoundsException.class, () -> oneOrderLinkedList.toArray(objects));
    }

    @Test
    void addShouldReturnTrueWhenNonNullElementAdded() {
        assertTrue(oneOrderLinkedList.add(1));
    }

    @Test
    void addShouldReturnTrueForEachNonNullElementAdded() {
        assertTrue(oneOrderLinkedList.add(1));
        assertTrue(oneOrderLinkedList.add(2));
        assertTrue(oneOrderLinkedList.add(3));
        assertTrue(oneOrderLinkedList.add(4));
    }

    @Test
    void addShouldThrowNullPointerExceptionWhenNullableElementPassed() {
        assertThrows(NullPointerException.class, ()-> oneOrderLinkedList.add(null));
    }

    @Test
    void removeShouldThrowIllegalStateExceptionWhenCollectionIsEmpty() {
        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.remove("a"));
    }

    @Test
    void removeShouldReturnTrueWhenExistedElementPassed() {
        oneOrderLinkedList.add(2);
        Object object = 2;
        assertTrue(oneOrderLinkedList.remove(object));
    }

    @Test
    void toArrayShouldReturnListOneSizeSmallerAfterRemovedWhenPassedExistedElement() {
        oneOrderLinkedList.add(2);
        oneOrderLinkedList.add(2);
        Object object = 2;
        oneOrderLinkedList.remove(object);
        assertEquals("[2]", Arrays.toString(oneOrderLinkedList.toArray()));
    }

    @Test
    void sizeShouldReturnZeroAfterRemovedOneExistingElement() {
        oneOrderLinkedList.add(2);
        Object object = 2;
        oneOrderLinkedList.remove(object);
        assertEquals(0, oneOrderLinkedList.size());
    }

    @Test
    void removeShouldReturnFalseWhenElementDidNotFind() {
        oneOrderLinkedList.add("a");
        assertFalse(oneOrderLinkedList.remove("b"));
    }

    @Test
    void containsAllShouldReturnFalseWhenCallableCollectionBiggerThanPassed() {
        List <Object> list = new ArrayList<>();
        oneOrderLinkedList.add(4);
        assertFalse(oneOrderLinkedList.containsAll(list));
    }

    @Test
    void containsAllShouldThrowNullPointerExceptionWhenNullCollectionPassed() {
        List <Object> list = null;
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);

        assertThrows(NullPointerException.class, () -> oneOrderLinkedList.containsAll(list));
    }

    @Test
    void containsAllShouldReturnFalseWhenPassedCollectionBiggerThanCallable() {
        List <Object> list = new ArrayList<>();
        list.add(5);
        assertFalse(oneOrderLinkedList.containsAll(list));
    }

    @Test
    void containsAllShouldReturnTrueWhenCollectionsSizeAndContentAreEquals() {
        List <Object> list = new ArrayList<>();
        list.add(5);
        oneOrderLinkedList.add(5);
        assertTrue(oneOrderLinkedList.containsAll(list));
    }

    @Test
    void containsAllShouldReturnFalseWhenValuesAreEqualsButElementCountsDifferent() {
        List <Object> list = new ArrayList<>();
        list.add(5);
        list.add(5);
        list.add(5);
        oneOrderLinkedList.add(5);
        oneOrderLinkedList.add(5);

        assertFalse(oneOrderLinkedList.containsAll(list));
    }

    @Test
    void containsAllShouldReturnFalseWhenCollectionsAreNotEqualsSizeSame() {
        List <Object> list = new ArrayList<>();
        list.add(5);
        list.add(3);
        oneOrderLinkedList.add(3);
        oneOrderLinkedList.add(8);
        assertFalse(oneOrderLinkedList.containsAll(list));
    }

    @Test
    void addAllShouldReturnFalseWhenAddedCollectionIsEmpty() {
        List <Object> list = new ArrayList<>();
        assertFalse(oneOrderLinkedList.addAll(list));
    }

    @Test
    void addAllShouldThrowNullPointerExceptionWhenNullCollectionPassed() {
        List <Object> list = null;
        assertThrows(NullPointerException.class, () -> oneOrderLinkedList.addAll(list));
    }

    @Test
    void addAllShouldReturnTrueWhenPassedCollectionIsNotEmpty() {
        List <Object> list = new ArrayList<>();
        list.add(5);
        assertTrue(oneOrderLinkedList.addAll(list));
    }

    @Test
    void addAllShouldReturnTrueWhenBothCollectionContainElements() {
        List <Object> list = new ArrayList<>();
        list.add(5);
        list.add(6);
        oneOrderLinkedList.add(3);
        oneOrderLinkedList.add(4);
        assertTrue(oneOrderLinkedList.addAll(list));
    }

    @Test
    void addAllShouldThrowNullPointerExceptionWhenNullCollectionPassed_withIndex() {
        List <Object> list = null;
        assertThrows(NullPointerException.class, () -> oneOrderLinkedList.addAll(0, list));
    }

    @Test
    void addAllShouldReturnFalseWhenAddedCollectionIsEmptyAndIndexIsAtBounds() {
        List <Object> list = new ArrayList<>();
        assertFalse(oneOrderLinkedList.addAll(0, list));
    }

    @Test
    void addAllShouldThrowIndexOutOfBoundsExceptionWhenIndexBiggerThanCollectionSize() {
        List <Object> list = new ArrayList<>();
        list.add(5);
        oneOrderLinkedList.add(10);

        assertThrows(IndexOutOfBoundsException.class, () -> oneOrderLinkedList.addAll(5, list));
    }

    @Test
    void addAllShouldReturnTrueWhenCollectionIsNotEmptyAndIndexIsAtBounds() {
        List <Object> list = new ArrayList<>();
        list.add(3);
        list.add(4);
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);
        assertTrue(oneOrderLinkedList.addAll(2, list));
    }

    @Test
    void removeAllShouldThrowIllegalStateExceptionWhenCallableCollectionIsEmpty() {
        List<Object> list = new ArrayList<>();
        list.add("a");

        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.removeAll(list));
    }

    @Test
    void removeAllShouldThrowIllegalStateExceptionWhenPassedCollectionIsEmpty() {
        List<Object> list = new ArrayList<>();
        oneOrderLinkedList.add("a");

        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.removeAll(list));
    }

    @Test
    void removeAllShouldThrowNullPointerExceptionWhenNullCollectionPassed() {
        List<Object> list = null;
        oneOrderLinkedList.add("a");

        assertThrows(NullPointerException.class, () -> oneOrderLinkedList.removeAll(list));
    }

    @Test
    void removeAllShouldReturnFalseWhenCallableCollectionDoesNotContainElementsFromPassed() {
        List<Object> list = new ArrayList<>();
        oneOrderLinkedList.add("a");
        oneOrderLinkedList.add("b");
        list.add("c");

        assertFalse(oneOrderLinkedList.removeAll(list));
    }

    @Test
    void removeAllShouldReturnTrueWhenCallableCollectionContainsElementsFromPassed() {
        List<Object> list = new ArrayList<>();
        oneOrderLinkedList.add("a");
        oneOrderLinkedList.add("b");
        list.add("a");
        list.add("b");

        assertTrue(oneOrderLinkedList.removeAll(list));
    }

    @Test
    void toArrayShouldReturnListWithoutValuesPassedToRemoveAll() {
        List<Object> list = new ArrayList<>();
        oneOrderLinkedList.add("a");
        oneOrderLinkedList.add("a");
        oneOrderLinkedList.add("b");
        list.add("a");
        oneOrderLinkedList.removeAll(list);

        assertEquals("[b]", Arrays.toString(oneOrderLinkedList.toArray()));
    }


    @Test
    void retainAllShouldThrowIllegalStateExceptionWhenPassedCollectionIsEmpty() {
        List<Object> list = new ArrayList<>();
        oneOrderLinkedList.add(5);

        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.retainAll(list));
    }

    @Test
    void retainAllShouldThrowIllegalStateExceptionWhenCallableCollectionIsEmpty() {
        List<Object> list = new ArrayList<>();
        list.add(5);

        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.retainAll(list));
    }

    @Test
    void retainAllShouldReturnFalseWhenCallableCollectionDoesNotContainElementsFromPassed() {
        List<Object> list = new ArrayList<>();
        list.add(5);
        list.add(6);
        oneOrderLinkedList.add(7);
        oneOrderLinkedList.add(8);

        assertFalse(oneOrderLinkedList.retainAll(list));
    }

    @Test
    void retainAllShouldReturnTrueWhenCallableCollectionContainsElementsFromPassed() {
        List<Object> list = new ArrayList<>();
        list.add(5);
        list.add(6);
        oneOrderLinkedList.add(5);
        oneOrderLinkedList.add(6);

        assertTrue(oneOrderLinkedList.retainAll(list));
    }

    @Test
    void toArrayShouldReturnListWithoutElementsFromPassedListInRetainAll() {
        List<Object> list = new ArrayList<>();
        list.add(5);
        list.add(6);
        oneOrderLinkedList.add(5);
        oneOrderLinkedList.add(5);
        oneOrderLinkedList.retainAll(list);

        assertEquals("[5]", Arrays.toString(oneOrderLinkedList.toArray()));
    }

    @Test
    void toArrayShouldReturnEmptyArrayAfterClear() {
        oneOrderLinkedList.clear();
        assertEquals("[]", Arrays.toString(oneOrderLinkedList.toArray()));
    }

    @Test
    void toArrayShouldReturnEmptyArrayAfterClear_collectionHasManyElements() {
        oneOrderLinkedList.add(5);
        oneOrderLinkedList.add(6);
        oneOrderLinkedList.clear();
        assertEquals("[]", Arrays.toString(oneOrderLinkedList.toArray()));
    }

    @Test
    void getShouldReturnElementFromFirstPositionWhenZeroPassed() {
        oneOrderLinkedList.add("a");
        oneOrderLinkedList.add("b");

        assertEquals("a", oneOrderLinkedList.get(0));
    }

    @Test
    void getShouldThrowIllegalStateExceptionWhenCollectionIsEmpty() {
        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.get(0));
    }

    @Test
    void getShouldThrowIndexOutOfBoundsExceptionWhenIndexBiggerThanCollectionSize() {
        oneOrderLinkedList.add("a");
        oneOrderLinkedList.add("b");
        oneOrderLinkedList.add("c");

        assertThrows(IndexOutOfBoundsException.class, () -> oneOrderLinkedList.get(3));
    }

    @Test
    void setShouldThrowIllegalStateExceptionWhenCollectionIsEmpty() {
        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.set(0,"A"));
    }

    @Test
    void setShouldThrowIndexOutOfBoundsExceptionWhenIndexBiggerThanCollectionSize() {
        oneOrderLinkedList.add("a");
        oneOrderLinkedList.add("b");
        oneOrderLinkedList.add("c");

        assertThrows(IndexOutOfBoundsException.class, () -> oneOrderLinkedList.set(4, "E"));
    }

    @Test
    void setShouldSetNewValueToTheFirstElementAndReturnItFromCollection() {
        oneOrderLinkedList.add("a");
        assertEquals("A", oneOrderLinkedList.set(0, "A"));
    }

    @Test
    void setShouldSetNewValueToCertainElementAndReturnIt() {
        oneOrderLinkedList.add("a");
        oneOrderLinkedList.add("b");
        oneOrderLinkedList.add("b");
        oneOrderLinkedList.add("d");
        assertEquals("c", oneOrderLinkedList.set(2, "c"));
    }

    @Test
    void addWithIndexShouldThrowNullPointerExceptionWhenNullableElementPassed() {
        assertThrows(NullPointerException.class, () -> oneOrderLinkedList.add(0, null));
    }

    @Test
    void addWithIndexShouldThrowIllegalArgumentExceptionWhenPassedIndexIsBelowZero() {
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);

        assertThrows(IllegalArgumentException.class, () -> oneOrderLinkedList.add(-1, "a"));
    }

    @Test
    void addWithIndexShouldThrowIndexOutOfBoundsExceptionWhenIndexBiggerThanCollectionSize() {
        oneOrderLinkedList.add(5);
        oneOrderLinkedList.add(6);
        assertThrows(IndexOutOfBoundsException.class, () -> oneOrderLinkedList.add(4, "AAA"));
    }

    @Test
    void removeWithIndexShouldThrowIllegalStateExceptionWhenCollectionIsEmpty() {
        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.remove(0));
    }

    @Test
    void removeWithIndexShouldThrowIndexOutOfBoundsExceptionWhenIndexBiggerThanCollectionSize() {
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);

        assertThrows(IndexOutOfBoundsException.class, () -> oneOrderLinkedList.remove(5));
    }

    @Test
    void removeWithIndexShouldThrowIllegalArgumentExceptionWhenPassedIndexIsBelowZero() {
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);

        assertThrows(IllegalArgumentException.class, () -> oneOrderLinkedList.remove(-1));
    }

    @Test
    void removeWithIndexShouldRemoveAndReturnFirstElementFromCollectionWhenZeroPassed() {
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);

        assertEquals(1, oneOrderLinkedList.remove(0));
    }

    @Test
    void indexOfShouldThrowIllegalStateExceptionWhenCallableCollectionIsEmpty() {
        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.indexOf("a"));
    }

    @Test
    void indexOfShouldReturnMinusOneWhenPassedElementDidNotFind() {
        oneOrderLinkedList.add(0);

        assertEquals(-1, oneOrderLinkedList.indexOf(null));
    }

    @Test
    void indexOfShouldReturnIndexOfPassedElementWhenItFound() {
        oneOrderLinkedList.add(0);
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);

        assertEquals(1, oneOrderLinkedList.indexOf(1));
    }

    @Test
    void lastIndexOfShouldThrowIllegalStateExceptionWhenCallableCollectionIsEmpty() {
        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.lastIndexOf("a"));
    }

    @Test
    void lastIndexOfShouldReturnMinusOneWhenPassedElementDidNotFind() {
        oneOrderLinkedList.add(0);

        assertEquals(-1, oneOrderLinkedList.lastIndexOf(null));
    }

    @Test
    void lastIndexOfShouldReturnLastIndexOfEqualsElementThatPassed() {
        oneOrderLinkedList.add(0);
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(0);

        assertEquals(2, oneOrderLinkedList.lastIndexOf(0));
    }

    @Test
    void subListShouldThrowIllegalStateExceptionWhenCallableCollectionIsEmpty() {
        assertThrows(IllegalStateException.class, () -> oneOrderLinkedList.subList(0,2));
    }

    @Test
    void subListShouldThrowIllegalArgumentExceptionWhenPassedIndexesAreEquals() {
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);
        oneOrderLinkedList.add(3);
        oneOrderLinkedList.add(4);
        assertThrows(IllegalArgumentException.class, () -> oneOrderLinkedList.subList(2,2));
    }

    @Test
    void subListShouldThrowIllegalArgumentExceptionWhenFirstPassedIndexBiggerThanSecond() {
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);
        oneOrderLinkedList.add(3);
        oneOrderLinkedList.add(4);
        assertThrows(IllegalArgumentException.class, () -> oneOrderLinkedList.subList(3,1));
    }

    @Test
    void subListShouldThrowIndexOutOfBoundsExceptionWhenOneOfIndexesIsOutOfBounds() {
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);
        oneOrderLinkedList.add(3);
        oneOrderLinkedList.add(4);
        assertThrows(IndexOutOfBoundsException.class, () -> oneOrderLinkedList.subList(0,7));
    }

    @Test
    void toArrayShouldReturnValuesOfTwoElementsInBoundsFromCallableCollection() {
        oneOrderLinkedList.add(1);
        oneOrderLinkedList.add(2);
        oneOrderLinkedList.add(3);
        oneOrderLinkedList.add(4);

        assertEquals("[1, 2]", Arrays.toString(oneOrderLinkedList.subList(0,2).toArray()));
    }

    @Test
    void listIteratorShouldThrowUnsupportedOperationExceptionWhenCalled() {
        assertThrows(UnsupportedOperationException.class, () -> oneOrderLinkedList.listIterator());
    }

    @Test
    void listIteratorShouldThrowUnsupportedOperationExceptionWhenCalled_withParameter() {
        assertThrows(UnsupportedOperationException.class, () -> oneOrderLinkedList.listIterator(0));
    }
}