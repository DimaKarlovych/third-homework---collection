package com.mycollection.one_order_linked_list;

import lombok.NonNull;
import java.util.*;

public class OneOrderLinkedList<E> implements List<E> {

    /**
     * Pointer to first node.
     */

    private Node first;

    /**
     * Pointer to last node.
     */

    private Node last;

    /**
     * Quantity elements in collection.
     */

    private int size;

    /**
     * Constructs an empty list.
     */

    public OneOrderLinkedList() {
        first = null;
        last = null;
        size = 0;
    }

    private class Node {
        private E data;
        private Node next;

        Node(E data, Node next) {
            this.data = data;
            this.next = next;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "data=" + data +
                    ", next=" + next +
                    '}';
        }
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */

    public int size() {
        return size;
    }

    /**
     * @return <tt>true</tt> if this list contains no elements
     */

    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns {@code true} if this list contains the specified element.
     * More formally, returns {@code true} if and only if this list contains
     * at least one element {@code e} such that
     * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
     *
     * @param o element whose presence in this list is to be tested
     * @return {@code true} if this list contains the specified element
     */

    public boolean contains(Object o) {
        if(this.isEmpty()) {
            throw new IllegalStateException("Illegal state exception !!!");
        }

        Node curr = first;
        while(curr != null || curr == last) {
            if(curr.data.equals(o)) {
                return true;
            }
            curr = curr.next;
        }
        return false;
     }

    /**
     * @return an iterator over the elements in this list in proper sequence
     */

    public Iterator<E> iterator() {
        return new Iterator<E>() {
            int position = 0;

            public boolean hasNext() {
                return position < size;
            }

            public E next() {
                if(hasNext()) {
                    return get(position++);
                }
                return null;
            }
        };
    }

    /**
     * Returns an array containing all of the elements in this list
     * in proper sequence (from first to last element).
     *
     * <p>The returned array will be "safe" in that no references to it are
     * maintained by this list.  (In other words, this method must allocate
     * a new array).  The caller is thus free to modify the returned array.
     *
     * <p>This method acts as bridge between array-based and collection-based
     * APIs.
     *
     * @return an array containing all of the elements in this list
     *         in proper sequence
     */

    public Object[] toArray() {
        Object []e = new Object[size];
        Iterator<E> iterator = iterator();

        int i = 0;

        while(iterator.hasNext()) {
            e[i] = iterator.next();
            i++;
        }

        return e;
    }

    /**
     * Returns an array containing all of the elements in this list in
     * proper sequence (from first to last element); the runtime type of
     * the returned array is that of the specified array.  If the list fits
     * in the specified array, it is returned therein.  Otherwise, a new
     * array is allocated with the runtime type of the specified array and
     * the size of this list.
     *
     * <p>If the list fits in the specified array with room to spare (i.e.,
     * the array has more elements than the list), the element in the array
     * immediately following the end of the list is set to {@code null}.
     * (This is useful in determining the length of the list <i>only</i> if
     * the caller knows that the list does not contain any null elements.)
     *
     * <p>Like the {@link #toArray()} method, this method acts as bridge between
     * array-based and collection-based APIs.  Further, this method allows
     * precise control over the runtime type of the output array, and may,
     * under certain circumstances, be used to save allocation costs.
     *
     * <p>Suppose {@code x} is a list known to contain only strings.
     * The following code can be used to dump the list into a newly
     * allocated array of {@code String}:
     *
     * <pre>
     *     String[] y = x.toArray(new String[0]);</pre>
     *
     * Note that {@code toArray(new Object[0])} is identical in function to
     * {@code toArray()}.
     *
     * @param ts the array into which the elements of the list are to
     *          be stored, if it is big enough; otherwise, a new array of the
     *          same runtime type is allocated for this purpose.
     * @return an array containing the elements of the list
     * @throws ArrayStoreException if the runtime type of the specified array
     *         is not a supertype of the runtime type of every element in
     *         this list
     * @throws NullPointerException if the specified array is null
     */

    public <T> T[] toArray(@NonNull T[] ts) {
        E []e = (E[]) toArray();

        if(ts.length > this.size) {
            throw new ArrayIndexOutOfBoundsException("Index out of bounds !!!");
        }

        System.arraycopy(e, 0, ts, 0, ts.length);

        return ts;
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param e element to be appended to this list
     * @return {@code true} (as specified by {@link Collection#add})
     */

    public boolean add(@NonNull E e) {
        if (!isEmpty()) {
            Node prev = last;
            last = new Node(e, null);
            prev.next = last;
        } else {
            last = new Node(e,null);
            first = last;
        }
        size++;

        return true;
    }

    /**
     * Removes the first occurrence of the specified element from this list,
     * if it is present.  If this list does not contain the element, it is
     * unchanged.  More formally, removes the element with the lowest index
     * {@code i} such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>
     * (if such an element exists).  Returns {@code true} if this list
     * contained the specified element (or equivalently, if this list
     * changed as a result of the call).
     *
     * @param o element to be removed from this list, if present
     * @return {@code true} if this list contained the specified element
     */

    public boolean remove(Object o) {
        if(isEmpty()) { throw new IllegalStateException("Can't remove from empty Collection!"); }

        boolean result = false;
        Node prev = first;
        Node curr = first;

        while (curr.next != null || curr == last) {
            if(curr.data.equals(o)) {
                if (size == 1) {
                    first = null;
                    last = null;
                } else if(curr.equals(first)) {
                    first = first.next;
                } else if(curr.equals(last)) {
                    last = prev;
                    last.next = null;
                } else {
                    prev.next = curr.next;
                }
                size--;
                result = true;
                break;
            }
            if(curr.next == null) {
                return false;
            }

            prev = curr;
            curr = prev.next;
        }
        return result;
    }

    /**
     * Returns <tt>true</tt> if this list contains all of the elements of the
     * specified collection.
     *
     * @param collection collection to be checked for containment in this list
     * @return <tt>true</tt> if this list contains all of the elements of the
     *         specified collection
     * @throws ClassCastException if the types of one or more elements
     *         in the specified collection are incompatible with this
     *         list
     * (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified collection contains one
     *         or more null elements and this list does not permit null
     *         elements
     *         (<a href="Collection.html#optional-restrictions">optional</a>),
     *         or if the specified collection is null
     * @see #contains(Object)
     */

    public boolean containsAll(Collection<?> collection) {
        if (collection.size() > size() || collection.size() < size()) {
            return false;
        }

        for (Object obj : this) {
            if (!collection.contains(obj)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Appends all of the elements in the specified collection to the end of
     * this list, in the order that they are returned by the specified
     * collection's iterator.  The behavior of this operation is undefined if
     * the specified collection is modified while the operation is in
     * progress.  (Note that this will occur if the specified collection is
     * this list, and it's nonempty.)
     *
     * @param collection collection containing elements to be added to this list
     * @return {@code true} if this list changed as a result of the call
     * @throws NullPointerException if the specified collection is null
     */

    public boolean addAll(@NonNull Collection<? extends E> collection) {
        if(collection.size() == 0) {
            return false;
        }

        for (E e : collection) {
            add(e);
        }
        return true;
    }

    /**
     * Inserts all of the elements in the specified collection into this
     * list, starting at the specified position.  Shifts the element
     * currently at that position (if any) and any subsequent elements to
     * the right (increases their indices).  The new elements will appear
     * in the list in the order that they are returned by the
     * specified collection's iterator.
     *
     * @param i index at which to insert the first element
     *              from the specified collection
     * @param collection collection containing elements to be added to this list
     * @return {@code true} if this list changed as a result of the call
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @throws NullPointerException if the specified collection is null
     */

    public boolean addAll(int i, @NonNull Collection<? extends E> collection) {
        if(i < 0 || i > size) {
            throw new IndexOutOfBoundsException("Out of bounds exception!");
        }

        if(collection.size() == 0) {
            return false;
        }

        int j = i;

        for(E e : collection) {
            add(j, e);
            j++;
        }
        return true;
    }

    /**
     * Removes from this list all of its elements that are contained in the
     * specified collection (optional operation).
     *
     * @param collection collection containing elements to be removed from this list
     * @return <tt>true</tt> if this list changed as a result of the call
     * @throws UnsupportedOperationException if the <tt>removeAll</tt> operation
     *         is not supported by this list
     * @throws ClassCastException if the class of an element of this list
     *         is incompatible with the specified collection
     * (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if this list contains a null element and the
     *         specified collection does not permit null elements
     *         (<a href="Collection.html#optional-restrictions">optional</a>),
     *         or if the specified collection is null
     * @see #remove(Object)
     * @see #contains(Object)
     */

    public boolean removeAll(@NonNull Collection<?> collection) {
        if(collection.isEmpty() || this.isEmpty()) {
            throw new IllegalStateException("Illegal state exception");
        }

        Iterator <?> iterator = collection.iterator();
        Iterator <?> iterator1 = this.iterator();

        boolean removeStatus = false;
        boolean result = false;

        while(iterator.hasNext()) {
            Object obj = iterator.next();
            while(iterator1.hasNext()) {
                Object obj2 = iterator1.next();
                if(obj.equals(obj2)) {
                    remove(obj2);
                    removeStatus = true;
                    result = true;
                }
                if(removeStatus) {
                    iterator1 = this.iterator();
                }
                removeStatus = false;
            }
            iterator1 = this.iterator();
        }
        return result;
    }

    /**
     * Retains only the elements in this list that are contained in the
     * specified collection (optional operation).  In other words, removes
     * from this list all of its elements that are not contained in the
     * specified collection.
     *
     * @param collection collection containing elements to be retained in this list
     * @return <tt>true</tt> if this list changed as a result of the call
     * @throws UnsupportedOperationException if the <tt>retainAll</tt> operation
     *         is not supported by this list
     * @throws ClassCastException if the class of an element of this list
     *         is incompatible with the specified collection
     * (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if this list contains a null element and the
     *         specified collection does not permit null elements
     *         (<a href="Collection.html#optional-restrictions">optional</a>),
     *         or if the specified collection is null
     * @see #remove(Object)
     * @see #contains(Object)
     */

    public boolean retainAll(Collection<?> collection) {
        if(this.isEmpty() || collection.isEmpty()) {
            throw new IllegalStateException("Illegal State Exception");
        }

        boolean result = false;

        for (Object obj : collection) {
            if (this.contains(obj)) {
                result = true;
                this.remove(obj);
            }
        }
        return result;
    }

    /**
     * Removes all of the elements from this list.
     * The list will be empty after this call returns.
     */

    public void clear() {
        if(!isEmpty()) {
            Node prev = first;
            Node curr = first;

            while (curr.next != null || curr == last || size == 0) {
                if (size == 0) {
                    return;
                }
                remove(0);
                curr = prev.next;
            }
        }
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param i index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */

    public E get(int i) {
        if(isEmpty()) { throw new IllegalStateException("Can't remove from empty Collection!"); }
        if(i < 0 || i > size) { throw new IndexOutOfBoundsException("Out of bounds!!!"); }

        Node prev;
        Node curr = first;

        int counter = 0;

        while (curr.next != null || curr == last) {
            if(counter == i) {
                return curr.data;
            }
            if(curr.next == null) {
                return get(-1);
            }
            prev = curr;
            curr = prev.next;
            counter++;
        }
        return null;
    }

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     *
     * @param i index of the element to replace
     * @param e element to be stored at the specified position
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */

    public E set(int i, E e) {
        if(isEmpty()) { throw new IllegalStateException("Can't remove from empty Collection!"); }

        if(i < 0 || i > size) { throw new IndexOutOfBoundsException("Out of bounds!!!"); }

        Node prev;
        Node curr = first;

        int counter = 0;

        while (curr.next != null || curr == last) {
            if(counter == i) {
                curr.data = e;
                return curr.data;
            }
            if(curr.next == null) {
                return get(-1);
            }
            prev = curr;
            curr = prev.next;
            counter++;
        }
        return null;
    }

    /**
     * Inserts the specified element at the specified position in this list.
     * Shifts the element currently at that position (if any) and any
     * subsequent elements to the right (adds one to their indices).
     *
     * @param i index at which the specified element is to be inserted
     * @param e element to be inserted
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */

    public void add(int i, @NonNull E e) {
        if(i < 0) {
            throw new IllegalArgumentException("Illegal argument exception !!!");
        }

        if(i > size) { throw new IndexOutOfBoundsException("Out of bounds!!!"); }

        if (size == i) {
            add(e);
            return;
        }

        Node curr = first;

        if(i == 0) {
            first = new Node(e, curr);
            size++;
            return;
        }

        int previousElementPosition = 0;
        while(previousElementPosition != (i-1)) {
            curr = curr.next;
            previousElementPosition++;
        }

        Node nextNode = curr.next;
        curr.next = new Node(e, nextNode);

        size++;
    }

    /**
     * Removes the element at the specified position in this list.  Shifts any
     * subsequent elements to the left (subtracts one from their indices).
     * Returns the element that was removed from the list.
     *
     * @param i the index of the element to be removed
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */

    public E remove(int i) {
        if(i < 0) {
            throw new IllegalArgumentException("Illegal argument exception !!!");
        }

        if(isEmpty()) { throw new IllegalStateException("Can't remove from empty Collection!"); }

        if(i > size) { throw new IndexOutOfBoundsException("Out of bounds!!!"); }

        Node prev = first;
        Node curr = first;

        int counter = 0;

        while (curr.next != null || curr == last) {
            if(counter == i) {
                if (size == 1) {
                    first = null;
                    last = null;
                } else if(curr.equals(first)) {
                    first = first.next;
                } else if(curr.equals(last)) {
                    last = prev;
                    last.next = null;
                } else {
                    prev.next = curr.next;
                }
                size--;
                return prev.data;
            }
            if(curr.next == null) {
                return get(-1);
            }
            prev = curr;
            curr = prev.next;

            counter++;
        }
        return null;
    }

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     * More formally, returns the lowest index {@code i} such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such index.
     *
     * @param o element to search for
     * @return the index of the first occurrence of the specified element in
     *         this list, or -1 if this list does not contain the element
     */

    public int indexOf(Object o) {
        if(this.isEmpty()) {
            throw new IllegalStateException("Illegal state exception");
        }

        int index = 0;

        Node curr = first;

        while(curr != null || curr == last) {
            if(curr.data.equals(o)) {
                return index;
            }
            curr = curr.next;
            index++;
        }
        return -1;
    }

    /**
     * Returns the index of the last occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     * More formally, returns the highest index {@code i} such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such index.
     *
     * @param o element to search for
     * @return the index of the last occurrence of the specified element in
     *         this list, or -1 if this list does not contain the element
     */

    public int lastIndexOf(Object o) {
        if(this.isEmpty()) {
            throw new IllegalStateException("Illegal state exception");
        }

        int index = 0;
        int lastIndex = -1;

        Node curr = first;

        while(curr != null || curr == last) {
            if(curr.data.equals(o)) {
                lastIndex = index;
            }
            curr = curr.next;
            index++;
        }
        return lastIndex;
    }

    /**
     * Returns a view of the portion of this list between the specified
     * <tt>fromIndex</tt>, inclusive, and <tt>toIndex</tt>, exclusive.  (If
     * <tt>fromIndex</tt> and <tt>toIndex</tt> are equal, the returned list is
     * empty.)  The returned list is backed by this list, so non-structural
     * changes in the returned list are reflected in this list, and vice-versa.
     * The returned list supports all of the optional list operations supported
     * by this list.<p>
     *
     * This method eliminates the need for explicit range operations (of
     * the sort that commonly exist for arrays).  Any operation that expects
     * a list can be used as a range operation by passing a subList view
     * instead of a whole list.  For example, the following idiom
     * removes a range of elements from a list:
     * <pre>{@code
     *      list.subList(from, to).clear();
     * }</pre>
     * Similar idioms may be constructed for <tt>indexOf</tt> and
     * <tt>lastIndexOf</tt>, and all of the algorithms in the
     * <tt>Collections</tt> class can be applied to a subList.<p>
     *
     * The semantics of the list returned by this method become undefined if
     * the backing list (i.e., this list) is <i>structurally modified</i> in
     * any way other than via the returned list.  (Structural modifications are
     * those that change the size of this list, or otherwise perturb it in such
     * a fashion that iterations in progress may yield incorrect results.)
     *
     * @param i low endpoint (inclusive) of the subList
     * @param i1 high endpoint (exclusive) of the subList
     * @return a view of the specified range within this list
     * @throws IndexOutOfBoundsException for an illegal endpoint index value
     *         (<tt>fromIndex &lt; 0 || toIndex &gt; size ||
     *         fromIndex &gt; toIndex</tt>)
     */

    public List<E> subList(int i, int i1) {
        if(this.isEmpty()) {
            throw new IllegalStateException("Collection is empty !!!");
        }

        if(i < 0 || i1 > size() || i1 < 0 || i > size ()) {
            throw new IndexOutOfBoundsException("Out of bounds!!!");
        }

        if (i == i1 || i > i1) {
            throw new IllegalArgumentException();
        }

        List<E> collection = new OneOrderLinkedList<>();
        Iterator iterator = this.iterator();

        int count = 0;

        while(iterator.hasNext()) {
            Object obj = iterator.next();
            if(count >= i && count <  i1) {
                collection.add((E) obj);
            }
            count++;
        }
        return collection;
    }

    /**
     * @return a list iterator over the elements in this list (in proper
     *         sequence)
     */

    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence), starting at the specified position in the list.
     * The specified index indicates the first element that would be
     * returned by an initial call to {@link ListIterator#next next}.
     * An initial call to {@link ListIterator#previous previous} would
     * return the element with the specified index minus one.
     *
     * @param i index of the first element to be returned from the
     *        list iterator (by a call to {@link ListIterator#next next})
     * @return a list iterator over the elements in this list (in proper
     *         sequence), starting at the specified position in the list
     * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index > size()})
     */

    public ListIterator<E> listIterator(int i) {
        throw new UnsupportedOperationException();
    }
}
